import express,{Express, Request, Response} from "express";
import dotenv from 'dotenv';

// Configuration the .env file
dotenv.config();

// Create Express APP
const app:  Express= express ();
const port: string | number = process.env.PORT || 8000;

//Define the firts Route of APP
app.get ('/', (req:Request, res:Response) => {
   //Send hello world
   res.send('Welcome to App Restful Express + TS + Nodemon + Jest+ Swagger + Monogoose');
});

//Define the firts Route of APP
app.get ('/hello', (req:Request, res:Response) => {
   //Send hello world
   res.send('Welcome to GET: Hello');
});

// Execute APP and Listen Requests to PORT
app.listen (port, ()=> {
   console.log (`EXPRESS SERVER: Running at http://localhost:${port}`)
})